import sys
import os
import json

def totalSeqlen(dir_path):
    
    total = "Error"
    try:
        if os.path.isdir(dir_path):
            seq_total = 0
            listoffiles = get_json_files(dir_path)

            if len(listoffiles) > 0:
              for f in listoffiles:
                with  open(f,"r") as data_file:
                    print("Json File    : " +f)
                    for data in data_file:
                        obj = json.loads(data)
                        seq_total=seq_total + obj["seqlen"]  
            else:
                print("No json files exists")
                total = 0 

            total = seq_total
        else:
            print("No dir exists, check the dir path")
            total = 0    
    except ValueError as ve:
        print("Execpetion "+ve)
    finally:
        return total

# Get the list of all json files from dir
def get_json_files(path):
    listoffiles =[]
    for root, dirs, files in os.walk(path):
          for json_file in files:
            if '.data.json' in json_file:
               listoffiles.append(os.path.join(root, json_file))
    return listoffiles

if __name__ == '__main__':
  if (len(sys.argv) > 2):
    total = totalSeqlen(sys.argv[2]) 
    if total > 0:
        print("Total of seqlen :",total)
  else:
    print("Argument(s) missing")  